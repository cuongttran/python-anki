from os import listdir
from os.path import isfile

from pydub import AudioSegment
from pydub.silence import split_on_silence
import sys

input_path = raw_input('Audio folder: ') or sys.path[0]
if not input_path.endswith('/'):
    input_path = input_path + '/'

output_path = raw_input('Output folder: ') or sys.path[0]
if not output_path.endswith('/'):
    output_path = output_path + '/'

output_ext = raw_input('Output extension (mp3/wav): ') or 'mp3'

mode = raw_input('Mode (1 - default parameters, 2 - advance configuration): ') or 1

min_silence_len = 1000
silence_thresh = -55
silence_duration = 100
normalized = -2.0

if mode == 1:
    print ("Default params: ")
    print ("Minimum silence length: 1000ms")
    print ("Silence threshold: -55")
    print ("Silence padding: 100ms")
    print ("Normalized: -2.0dB")
elif mode == 2:
    min_silence_len = raw_input('Minimum silence length (in ms) (default: 1000): ') or 1000
    silence_thresh = raw_input('Silence threshold (default: -55): ') or -55
    silence_duration = raw_input('Silence padding (in ms) (default: 100): ') or 100
    normalized = raw_input('Normalized (default: -2.0dB): ') or -2.0

files = [f for f in listdir(input_path) if isfile(input_path + f) and f.endswith(('.mp3', '.wav'))]
print ("Files:")
for f in files:
    print(input_path + f)


def match_target_amplitude(aChunk, target_dBFS):
    change_in_dBFS = target_dBFS - aChunk.max_dBFS
    return aChunk.apply_gain(change_in_dBFS)


for f in files:
    print ('Processing: {}'.format(input_path + f))

    if f.endswith('.mp3'):
        audio = AudioSegment.from_mp3(input_path + f)
    elif f.endswith('.wav'):
        audio = AudioSegment.from_wav(input_path + f)
    fn = f[:-4]

    chunks = split_on_silence(audio, min_silence_len=min_silence_len, silence_thresh=silence_thresh)

    # Process each chunk per requirements
    for i, chunk in enumerate(chunks):
        silence_chunk = AudioSegment.silent(duration=silence_duration)
        audio_chunk = silence_chunk + chunk + silence_chunk

        # Normalize each audio chunk
        normalized_chunk = match_target_amplitude(audio_chunk, normalized)

        # Export audio chunk with new bitrate
        print("Exporting {0}{1}-{2}".format(output_path, fn, i))
        if output_ext == 'mp3':
            normalized_chunk.export("{0}{1}-{2}.mp3".format(output_path, fn, i), bitrate='192k', format="mp3")
        elif output_ext == 'wav':
            normalized_chunk.export("{0}{1}-{2}.wav".format(output_path, fn, i), bitrate='192k', format="wav")
